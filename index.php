<?php

class User{
    
    private $nev;
    private $email;
    
    public function __construct($nev, $email){
        $this->nev=$nev;
        $this->email=$email;
    }
    
    public function __get($peldany){
        return $this->$peldany;
    }
    
    public function __set($key, $value){
        $this->$key=$value;
    }
    
    public function __toString(){
        return "<b>User{</b>'Név':'".$this->nev."','Email':'".$this->email."'<b>}</b>";
    }
    
}

class Task{
    
    private $leiras;
    private $felelos;
    private $hatarido;
    
    public function __construct($leiras, User $felelos, DateTime $hatarido){
        $this->leiras=$leiras;
        $this->felelos=$felelos;
        $this->hatarido=$hatarido;
    }
    
    public function __get($peldany){
        return $this->$peldany;
    }
    
    public function __set($key, $value){
        $this->$key=$value;
    }
    
    protected function forma(){
        return $this->hatarido->format('Y.m.d');
    }
    
    public function __toString(){
        return "<b>Task{</b>'Leírás': '".$this->leiras."','Felelős':'".$this->felelos."','Határidő':'".$this->forma()."'<b>}</b>";
    }
    
}

$leiras="Valami leírás";
$felelos=new User("Név","Email");

$most=new DateTime();
$hatarido=$most->modify('+1 day');

$task=new Task($leiras,$felelos,$hatarido);

echo $task;

?>